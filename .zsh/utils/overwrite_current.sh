#!/usr/bin/sh
set -e
cp ../zshrc ~/.zshrc
cp -r ../alias ~/.zsh
cp -r ../cosmetic ~/.zsh
cp ../env/zshrc ../env/secrets ~/.zsh/env
